### 16.1.2 Enable email notifications for pools upon building their sandboxes.
* ff6dae8 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9d669e0 -- Merge branch '44-add-email-notifications-to-pools' into 'master'
* dc927ee -- Enable email notifications for pools
### 16.1.1 Add option to make pools private and comment on their allocation units instead of instances.
* 7ae8b9d -- [CI/CD] Update packages.json version based on GitLab tag.
* 4523a98 -- Merge branch '43-update-pool-models-for-edition' into 'master'
* b1e5ca1 -- Add option to make pools private and comment on their allocation units instead of instances
### 16.0.0 Update to Angular 16.
* ea3c291 -- [CI/CD] Update packages.json version based on GitLab tag.
* 49f2b3d -- Merge branch '42-update-to-angular-16' into 'master'
* 8750200 -- Update to Angular 16
### 15.1.1 Add optional pool and sandbox instance attribute 'comment'.
* adad354 -- [CI/CD] Update packages.json version based on GitLab tag.
* f908729 -- Merge branch '41-add-comments-to-sandbox-pools-and-instances' into 'master'
* 7310451 -- Add optional pool and sandbox instance attribute 'comment'
### 15.1.0 Adjust SandboxAllocationUnit to enable corresponding requests as optional upon creation.
* a5d765c -- [CI/CD] Update packages.json version based on GitLab tag.
* f0fd9d7 -- Merge branch '40-update-sandbox-allocation-unit' into 'master'
* e040e50 -- Adjust SandboxAllocationUnit to enable requests as optional
### 15.0.0 Update to Angular 15.
* 6c7bbb8 -- [CI/CD] Update packages.json version based on GitLab tag.
* 6d98176 -- Merge branch '39-update-to-angular-15' into 'master'
* bac9cb1 -- Update to Angular 15
### 14.3.0 Add sandbox uuid to SandboxAllocationUnit and change id of SandboxInstance to string type.
* e853a28 -- [CI/CD] Update packages.json version based on GitLab tag.
* 70c94e5 -- Merge branch '38-change-sandbox-id-to-uuid-and-string-type' into 'master'
* bfb83f9 -- Resolve "Change sandbox id to uuid and string type"
### 14.2.0 Refactor of Resources page. Add information about version and gui access to virtual image. 
* 1714f7c -- [CI/CD] Update packages.json version based on GitLab tag.
* 8be80db -- Merge branch '37-refactor-resources-page' into 'master'
* 448b055 -- Resolve "Refactor resources page"
### 14.1.0 Adjust Pool model to address backend changes. Add info to allocation unit model about its state.
* 8d4df38 -- [CI/CD] Update packages.json version based on GitLab tag.
* b18c369 -- Merge branch '35-optimize-sandbox-agenda' into 'master'
* 9486dee -- Resolve "Optimize sandbox agenda"
### 14.0.0 Update to Angular 14.
* 2f6952d -- [CI/CD] Update packages.json version based on GitLab tag.
*   a709dfa -- Merge branch '34-release-v14' into 'master'
|\  
| * f2eb2fe -- Resolve "Release v14"
|/  
* 7b6324b -- Merge branch '33-update-to-angular-14' into 'master'
* 6d38260 -- Resolve "Update to Angular 14"
### 13.1.0 Replace openstack with terraform
* 521d685 -- [CI/CD] Update packages.json version based on GitLab tag.
*   a1ec5a8 -- Merge branch '32-replace-openstack-with-terraform' into 'master'
|\  
| * 5946b99 -- Resolve "Replace openstack with terraform"
|/  
* edad2cf -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 79ea913 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1aa1c4e -- Merge branch '30-update-to-angular-13' into 'master'
|\  
| * 9d66f7f -- Resolve "Update to Angular 13"
|/  
*   5dfdc4f -- Merge branch '31-add-retry-state-to-requeststage' into 'master'
|\  
| * 438d0c8 -- Resolve "Add Retry state to RequestStage"
|/  
* 8101782 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e4be854 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5d4b361 -- Merge branch '29-add-created-by-field-to-sd' into 'master'
|\  
| * 48771fd -- Resolve "Add created by field to SD"
|/  
* ad8743b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 401018b -- [CI/CD] Update packages.json version based on GitLab tag.
*   c3c7984 -- Merge branch '28-add-created-by-field-to-allocation-unit' into 'master'
|\  
| * 881f37f -- Resolve "Add created by field to allocation unit"
|/  
* 1790fc0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 05c519f -- [CI/CD] Update packages.json version based on GitLab tag.
*   5380c0f -- Merge branch '27-add-stages-and-resource-usage' into 'master'
|\  
| * 37fc002 -- Resolve "Add stages and resource usage"
|/  
*   b697758 -- Merge branch '26-add-license-file' into 'master'
|\  
| * a3c6437 -- Add license file
|/  
* 6e4d328 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fcb1185 -- [CI/CD] Update packages.json version based on GitLab tag.
*   638d2a2 -- Merge branch '25-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 1308694 -- Resolve "Add stage state to allocation unit"
|/  
* 76fb065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6c7c99c -- [CI/CD] Update packages.json version based on GitLab tag.
*   44c03e4 -- Merge branch '24-rollback-version' into 'master'
|\  
| * 8fba612 -- Update release message
|/  
*   bc00dbf -- Merge branch '23-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 4cd410b -- Update gitlab CI
|/  
* 4e4a557 -- Update project package.json version based on GitLab tag. Done by CI
*   89cc966 -- Merge branch '22-update-to-angular-12' into 'master'
|\  
| * 5413b5d -- Resolve "Update to Angular 12"
|/  
* 21711e5 -- Update project package.json version based on GitLab tag. Done by CI
*   25a5976 -- Merge branch '21-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 821c4be -- peerDependency update
|/  
* 4b2d2c0 -- Update project package.json version based on GitLab tag. Done by CI
*   278d08a -- Merge branch '20-update-to-angular-11' into 'master'
|\  
| * dcc5fb7 -- Resolve "Update to Angular 11"
|/  
*   628c498 -- Merge branch '19-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c016f61 -- recreate package-lock
|/  
* f0836c8 -- Update project package.json version based on GitLab tag. Done by CI
*   fa45b85 -- Merge branch '18-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * aa15285 -- Rename the package scope
|/  
*   4aa95d5 -- Merge branch '17-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d016790 -- Resolve "Migrate from tslint to eslint"
|/  
*   e2ef594 -- Merge branch '445720-master-patch-02194' into 'master'
|\  
| * 5ecd26b -- Update README.md
|/  
* e6f7783 -- Update project package.json version based on GitLab tag. Done by CI
*   6b7669f -- Merge branch '16-rename-package-to-kypo-sandbox-model' into 'master'
|\  
| * cff4c9d -- Rename the package
|/  
* f394fa7 -- Update project package.json version based on GitLab tag. Done by CI
*   27ba748 -- Merge branch '15-add-virtual-images-models' into 'master'
|\  
| * 822b5a3 -- Resolve "Add virtual images models"
|/  
* e3dce87 -- Update project package.json version based on GitLab tag. Done by CI
*   72a21f1 -- Merge branch '14-add-sandbox-resources-models' into 'master'
|\  
| * b133e56 -- Resolve "Add sandbox resources models"
|/  
*   56f4143 -- Merge branch '13-use-cypress-image-in-ci' into 'master'
|\  
| * 7fd6cfe -- Resolve "Use cypress image in CI"
|/  
* 3706d0a -- Update project package.json version based on GitLab tag. Done by CI
*   0818c2e -- Merge branch '12-refactor-to-use-sentinel-common-instead-of-kypo-common' into 'master'
|\  
| * 1516547 -- Remove dependency on kypo-common
|/  
*   7b48a39 -- Merge branch '11-remove-allocation-stage-id-from-openstack-cleanup-stage' into 'master'
|\  
| * 6dc6a30 -- Remove allocation stage id attr
|/  
* 67236b5 -- Update project package.json version based on GitLab tag. Done by CI
*   743f8ea -- Merge branch '10-add-new-stage-types' into 'master'
|\  
| * 72b3b4f -- Resolve "Add new stage types"
|/  
* 69ab271 -- Update project package.json version based on GitLab tag. Done by CI
*   1479e53 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 3fa2375 -- Resolve "Update to Angular 10"
|/  
* fbe7b31 -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
* b8158c7 -- Update .gitlab-ci.yml
### 13.0.0 Update to Angular 13, CI/CD optimization, add retry stage to request stage
* 79ea913 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1aa1c4e -- Merge branch '30-update-to-angular-13' into 'master'
|\  
| * 9d66f7f -- Resolve "Update to Angular 13"
|/  
*   5dfdc4f -- Merge branch '31-add-retry-state-to-requeststage' into 'master'
|\  
| * 438d0c8 -- Resolve "Add Retry state to RequestStage"
|/  
* 8101782 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e4be854 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5d4b361 -- Merge branch '29-add-created-by-field-to-sd' into 'master'
|\  
| * 48771fd -- Resolve "Add created by field to SD"
|/  
* ad8743b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 401018b -- [CI/CD] Update packages.json version based on GitLab tag.
*   c3c7984 -- Merge branch '28-add-created-by-field-to-allocation-unit' into 'master'
|\  
| * 881f37f -- Resolve "Add created by field to allocation unit"
|/  
* 1790fc0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 05c519f -- [CI/CD] Update packages.json version based on GitLab tag.
*   5380c0f -- Merge branch '27-add-stages-and-resource-usage' into 'master'
|\  
| * 37fc002 -- Resolve "Add stages and resource usage"
|/  
*   b697758 -- Merge branch '26-add-license-file' into 'master'
|\  
| * a3c6437 -- Add license file
|/  
* 6e4d328 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fcb1185 -- [CI/CD] Update packages.json version based on GitLab tag.
*   638d2a2 -- Merge branch '25-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 1308694 -- Resolve "Add stage state to allocation unit"
|/  
* 76fb065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6c7c99c -- [CI/CD] Update packages.json version based on GitLab tag.
*   44c03e4 -- Merge branch '24-rollback-version' into 'master'
|\  
| * 8fba612 -- Update release message
|/  
*   bc00dbf -- Merge branch '23-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 4cd410b -- Update gitlab CI
|/  
* 4e4a557 -- Update project package.json version based on GitLab tag. Done by CI
*   89cc966 -- Merge branch '22-update-to-angular-12' into 'master'
|\  
| * 5413b5d -- Resolve "Update to Angular 12"
|/  
* 21711e5 -- Update project package.json version based on GitLab tag. Done by CI
*   25a5976 -- Merge branch '21-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 821c4be -- peerDependency update
|/  
* 4b2d2c0 -- Update project package.json version based on GitLab tag. Done by CI
*   278d08a -- Merge branch '20-update-to-angular-11' into 'master'
|\  
| * dcc5fb7 -- Resolve "Update to Angular 11"
|/  
*   628c498 -- Merge branch '19-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c016f61 -- recreate package-lock
|/  
* f0836c8 -- Update project package.json version based on GitLab tag. Done by CI
*   fa45b85 -- Merge branch '18-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * aa15285 -- Rename the package scope
|/  
*   4aa95d5 -- Merge branch '17-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d016790 -- Resolve "Migrate from tslint to eslint"
|/  
*   e2ef594 -- Merge branch '445720-master-patch-02194' into 'master'
|\  
| * 5ecd26b -- Update README.md
|/  
* e6f7783 -- Update project package.json version based on GitLab tag. Done by CI
*   6b7669f -- Merge branch '16-rename-package-to-kypo-sandbox-model' into 'master'
|\  
| * cff4c9d -- Rename the package
|/  
* f394fa7 -- Update project package.json version based on GitLab tag. Done by CI
*   27ba748 -- Merge branch '15-add-virtual-images-models' into 'master'
|\  
| * 822b5a3 -- Resolve "Add virtual images models"
|/  
* e3dce87 -- Update project package.json version based on GitLab tag. Done by CI
*   72a21f1 -- Merge branch '14-add-sandbox-resources-models' into 'master'
|\  
| * b133e56 -- Resolve "Add sandbox resources models"
|/  
*   56f4143 -- Merge branch '13-use-cypress-image-in-ci' into 'master'
|\  
| * 7fd6cfe -- Resolve "Use cypress image in CI"
|/  
* 3706d0a -- Update project package.json version based on GitLab tag. Done by CI
*   0818c2e -- Merge branch '12-refactor-to-use-sentinel-common-instead-of-kypo-common' into 'master'
|\  
| * 1516547 -- Remove dependency on kypo-common
|/  
*   7b48a39 -- Merge branch '11-remove-allocation-stage-id-from-openstack-cleanup-stage' into 'master'
|\  
| * 6dc6a30 -- Remove allocation stage id attr
|/  
* 67236b5 -- Update project package.json version based on GitLab tag. Done by CI
*   743f8ea -- Merge branch '10-add-new-stage-types' into 'master'
|\  
| * 72b3b4f -- Resolve "Add new stage types"
|/  
* 69ab271 -- Update project package.json version based on GitLab tag. Done by CI
*   1479e53 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 3fa2375 -- Resolve "Update to Angular 10"
|/  
* fbe7b31 -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
* b8158c7 -- Update .gitlab-ci.yml
### 12.0.6 Add created by field to sandbox definition
* e4be854 -- [CI/CD] Update packages.json version based on GitLab tag.
*   5d4b361 -- Merge branch '29-add-created-by-field-to-sd' into 'master'
|\  
| * 48771fd -- Resolve "Add created by field to SD"
|/  
* ad8743b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 401018b -- [CI/CD] Update packages.json version based on GitLab tag.
*   c3c7984 -- Merge branch '28-add-created-by-field-to-allocation-unit' into 'master'
|\  
| * 881f37f -- Resolve "Add created by field to allocation unit"
|/  
* 1790fc0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 05c519f -- [CI/CD] Update packages.json version based on GitLab tag.
*   5380c0f -- Merge branch '27-add-stages-and-resource-usage' into 'master'
|\  
| * 37fc002 -- Resolve "Add stages and resource usage"
|/  
*   b697758 -- Merge branch '26-add-license-file' into 'master'
|\  
| * a3c6437 -- Add license file
|/  
* 6e4d328 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fcb1185 -- [CI/CD] Update packages.json version based on GitLab tag.
*   638d2a2 -- Merge branch '25-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 1308694 -- Resolve "Add stage state to allocation unit"
|/  
* 76fb065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6c7c99c -- [CI/CD] Update packages.json version based on GitLab tag.
*   44c03e4 -- Merge branch '24-rollback-version' into 'master'
|\  
| * 8fba612 -- Update release message
|/  
*   bc00dbf -- Merge branch '23-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 4cd410b -- Update gitlab CI
|/  
* 4e4a557 -- Update project package.json version based on GitLab tag. Done by CI
*   89cc966 -- Merge branch '22-update-to-angular-12' into 'master'
|\  
| * 5413b5d -- Resolve "Update to Angular 12"
|/  
* 21711e5 -- Update project package.json version based on GitLab tag. Done by CI
*   25a5976 -- Merge branch '21-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 821c4be -- peerDependency update
|/  
* 4b2d2c0 -- Update project package.json version based on GitLab tag. Done by CI
*   278d08a -- Merge branch '20-update-to-angular-11' into 'master'
|\  
| * dcc5fb7 -- Resolve "Update to Angular 11"
|/  
*   628c498 -- Merge branch '19-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c016f61 -- recreate package-lock
|/  
* f0836c8 -- Update project package.json version based on GitLab tag. Done by CI
*   fa45b85 -- Merge branch '18-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * aa15285 -- Rename the package scope
|/  
*   4aa95d5 -- Merge branch '17-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d016790 -- Resolve "Migrate from tslint to eslint"
|/  
*   e2ef594 -- Merge branch '445720-master-patch-02194' into 'master'
|\  
| * 5ecd26b -- Update README.md
|/  
* e6f7783 -- Update project package.json version based on GitLab tag. Done by CI
*   6b7669f -- Merge branch '16-rename-package-to-kypo-sandbox-model' into 'master'
|\  
| * cff4c9d -- Rename the package
|/  
* f394fa7 -- Update project package.json version based on GitLab tag. Done by CI
*   27ba748 -- Merge branch '15-add-virtual-images-models' into 'master'
|\  
| * 822b5a3 -- Resolve "Add virtual images models"
|/  
* e3dce87 -- Update project package.json version based on GitLab tag. Done by CI
*   72a21f1 -- Merge branch '14-add-sandbox-resources-models' into 'master'
|\  
| * b133e56 -- Resolve "Add sandbox resources models"
|/  
*   56f4143 -- Merge branch '13-use-cypress-image-in-ci' into 'master'
|\  
| * 7fd6cfe -- Resolve "Use cypress image in CI"
|/  
* 3706d0a -- Update project package.json version based on GitLab tag. Done by CI
*   0818c2e -- Merge branch '12-refactor-to-use-sentinel-common-instead-of-kypo-common' into 'master'
|\  
| * 1516547 -- Remove dependency on kypo-common
|/  
*   7b48a39 -- Merge branch '11-remove-allocation-stage-id-from-openstack-cleanup-stage' into 'master'
|\  
| * 6dc6a30 -- Remove allocation stage id attr
|/  
* 67236b5 -- Update project package.json version based on GitLab tag. Done by CI
*   743f8ea -- Merge branch '10-add-new-stage-types' into 'master'
|\  
| * 72b3b4f -- Resolve "Add new stage types"
|/  
* 69ab271 -- Update project package.json version based on GitLab tag. Done by CI
*   1479e53 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 3fa2375 -- Resolve "Update to Angular 10"
|/  
* fbe7b31 -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
* b8158c7 -- Update .gitlab-ci.yml
### 12.0.5 Add created by field to allocation unit
* 401018b -- [CI/CD] Update packages.json version based on GitLab tag.
*   c3c7984 -- Merge branch '28-add-created-by-field-to-allocation-unit' into 'master'
|\  
| * 881f37f -- Resolve "Add created by field to allocation unit"
|/  
* 1790fc0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 05c519f -- [CI/CD] Update packages.json version based on GitLab tag.
*   5380c0f -- Merge branch '27-add-stages-and-resource-usage' into 'master'
|\  
| * 37fc002 -- Resolve "Add stages and resource usage"
|/  
*   b697758 -- Merge branch '26-add-license-file' into 'master'
|\  
| * a3c6437 -- Add license file
|/  
* 6e4d328 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fcb1185 -- [CI/CD] Update packages.json version based on GitLab tag.
*   638d2a2 -- Merge branch '25-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 1308694 -- Resolve "Add stage state to allocation unit"
|/  
* 76fb065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6c7c99c -- [CI/CD] Update packages.json version based on GitLab tag.
*   44c03e4 -- Merge branch '24-rollback-version' into 'master'
|\  
| * 8fba612 -- Update release message
|/  
*   bc00dbf -- Merge branch '23-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 4cd410b -- Update gitlab CI
|/  
* 4e4a557 -- Update project package.json version based on GitLab tag. Done by CI
*   89cc966 -- Merge branch '22-update-to-angular-12' into 'master'
|\  
| * 5413b5d -- Resolve "Update to Angular 12"
|/  
* 21711e5 -- Update project package.json version based on GitLab tag. Done by CI
*   25a5976 -- Merge branch '21-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 821c4be -- peerDependency update
|/  
* 4b2d2c0 -- Update project package.json version based on GitLab tag. Done by CI
*   278d08a -- Merge branch '20-update-to-angular-11' into 'master'
|\  
| * dcc5fb7 -- Resolve "Update to Angular 11"
|/  
*   628c498 -- Merge branch '19-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c016f61 -- recreate package-lock
|/  
* f0836c8 -- Update project package.json version based on GitLab tag. Done by CI
*   fa45b85 -- Merge branch '18-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * aa15285 -- Rename the package scope
|/  
*   4aa95d5 -- Merge branch '17-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d016790 -- Resolve "Migrate from tslint to eslint"
|/  
*   e2ef594 -- Merge branch '445720-master-patch-02194' into 'master'
|\  
| * 5ecd26b -- Update README.md
|/  
* e6f7783 -- Update project package.json version based on GitLab tag. Done by CI
*   6b7669f -- Merge branch '16-rename-package-to-kypo-sandbox-model' into 'master'
|\  
| * cff4c9d -- Rename the package
|/  
* f394fa7 -- Update project package.json version based on GitLab tag. Done by CI
*   27ba748 -- Merge branch '15-add-virtual-images-models' into 'master'
|\  
| * 822b5a3 -- Resolve "Add virtual images models"
|/  
* e3dce87 -- Update project package.json version based on GitLab tag. Done by CI
*   72a21f1 -- Merge branch '14-add-sandbox-resources-models' into 'master'
|\  
| * b133e56 -- Resolve "Add sandbox resources models"
|/  
*   56f4143 -- Merge branch '13-use-cypress-image-in-ci' into 'master'
|\  
| * 7fd6cfe -- Resolve "Use cypress image in CI"
|/  
* 3706d0a -- Update project package.json version based on GitLab tag. Done by CI
*   0818c2e -- Merge branch '12-refactor-to-use-sentinel-common-instead-of-kypo-common' into 'master'
|\  
| * 1516547 -- Remove dependency on kypo-common
|/  
*   7b48a39 -- Merge branch '11-remove-allocation-stage-id-from-openstack-cleanup-stage' into 'master'
|\  
| * 6dc6a30 -- Remove allocation stage id attr
|/  
* 67236b5 -- Update project package.json version based on GitLab tag. Done by CI
*   743f8ea -- Merge branch '10-add-new-stage-types' into 'master'
|\  
| * 72b3b4f -- Resolve "Add new stage types"
|/  
* 69ab271 -- Update project package.json version based on GitLab tag. Done by CI
*   1479e53 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 3fa2375 -- Resolve "Update to Angular 10"
|/  
* fbe7b31 -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
* b8158c7 -- Update .gitlab-ci.yml
### 12.0.4 Add pool resource usage
* 05c519f -- [CI/CD] Update packages.json version based on GitLab tag.
*   5380c0f -- Merge branch '27-add-stages-and-resource-usage' into 'master'
|\  
| * 37fc002 -- Resolve "Add stages and resource usage"
|/  
*   b697758 -- Merge branch '26-add-license-file' into 'master'
|\  
| * a3c6437 -- Add license file
|/  
* 6e4d328 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fcb1185 -- [CI/CD] Update packages.json version based on GitLab tag.
*   638d2a2 -- Merge branch '25-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 1308694 -- Resolve "Add stage state to allocation unit"
|/  
* 76fb065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6c7c99c -- [CI/CD] Update packages.json version based on GitLab tag.
*   44c03e4 -- Merge branch '24-rollback-version' into 'master'
|\  
| * 8fba612 -- Update release message
|/  
*   bc00dbf -- Merge branch '23-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 4cd410b -- Update gitlab CI
|/  
* 4e4a557 -- Update project package.json version based on GitLab tag. Done by CI
*   89cc966 -- Merge branch '22-update-to-angular-12' into 'master'
|\  
| * 5413b5d -- Resolve "Update to Angular 12"
|/  
* 21711e5 -- Update project package.json version based on GitLab tag. Done by CI
*   25a5976 -- Merge branch '21-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 821c4be -- peerDependency update
|/  
* 4b2d2c0 -- Update project package.json version based on GitLab tag. Done by CI
*   278d08a -- Merge branch '20-update-to-angular-11' into 'master'
|\  
| * dcc5fb7 -- Resolve "Update to Angular 11"
|/  
*   628c498 -- Merge branch '19-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c016f61 -- recreate package-lock
|/  
* f0836c8 -- Update project package.json version based on GitLab tag. Done by CI
*   fa45b85 -- Merge branch '18-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * aa15285 -- Rename the package scope
|/  
*   4aa95d5 -- Merge branch '17-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d016790 -- Resolve "Migrate from tslint to eslint"
|/  
*   e2ef594 -- Merge branch '445720-master-patch-02194' into 'master'
|\  
| * 5ecd26b -- Update README.md
|/  
* e6f7783 -- Update project package.json version based on GitLab tag. Done by CI
*   6b7669f -- Merge branch '16-rename-package-to-kypo-sandbox-model' into 'master'
|\  
| * cff4c9d -- Rename the package
|/  
* f394fa7 -- Update project package.json version based on GitLab tag. Done by CI
*   27ba748 -- Merge branch '15-add-virtual-images-models' into 'master'
|\  
| * 822b5a3 -- Resolve "Add virtual images models"
|/  
* e3dce87 -- Update project package.json version based on GitLab tag. Done by CI
*   72a21f1 -- Merge branch '14-add-sandbox-resources-models' into 'master'
|\  
| * b133e56 -- Resolve "Add sandbox resources models"
|/  
*   56f4143 -- Merge branch '13-use-cypress-image-in-ci' into 'master'
|\  
| * 7fd6cfe -- Resolve "Use cypress image in CI"
|/  
* 3706d0a -- Update project package.json version based on GitLab tag. Done by CI
*   0818c2e -- Merge branch '12-refactor-to-use-sentinel-common-instead-of-kypo-common' into 'master'
|\  
| * 1516547 -- Remove dependency on kypo-common
|/  
*   7b48a39 -- Merge branch '11-remove-allocation-stage-id-from-openstack-cleanup-stage' into 'master'
|\  
| * 6dc6a30 -- Remove allocation stage id attr
|/  
* 67236b5 -- Update project package.json version based on GitLab tag. Done by CI
*   743f8ea -- Merge branch '10-add-new-stage-types' into 'master'
|\  
| * 72b3b4f -- Resolve "Add new stage types"
|/  
* 69ab271 -- Update project package.json version based on GitLab tag. Done by CI
*   1479e53 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 3fa2375 -- Resolve "Update to Angular 10"
|/  
* fbe7b31 -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
* b8158c7 -- Update .gitlab-ci.yml
### 12.0.3 Add state of stages to allocation unit
* fcb1185 -- [CI/CD] Update packages.json version based on GitLab tag.
*   638d2a2 -- Merge branch '25-add-stage-state-to-allocation-unit' into 'master'
|\  
| * 1308694 -- Resolve "Add stage state to allocation unit"
|/  
* 76fb065 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6c7c99c -- [CI/CD] Update packages.json version based on GitLab tag.
*   44c03e4 -- Merge branch '24-rollback-version' into 'master'
|\  
| * 8fba612 -- Update release message
|/  
*   bc00dbf -- Merge branch '23-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 4cd410b -- Update gitlab CI
|/  
* 4e4a557 -- Update project package.json version based on GitLab tag. Done by CI
*   89cc966 -- Merge branch '22-update-to-angular-12' into 'master'
|\  
| * 5413b5d -- Resolve "Update to Angular 12"
|/  
* 21711e5 -- Update project package.json version based on GitLab tag. Done by CI
*   25a5976 -- Merge branch '21-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 821c4be -- peerDependency update
|/  
* 4b2d2c0 -- Update project package.json version based on GitLab tag. Done by CI
*   278d08a -- Merge branch '20-update-to-angular-11' into 'master'
|\  
| * dcc5fb7 -- Resolve "Update to Angular 11"
|/  
*   628c498 -- Merge branch '19-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c016f61 -- recreate package-lock
|/  
* f0836c8 -- Update project package.json version based on GitLab tag. Done by CI
*   fa45b85 -- Merge branch '18-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * aa15285 -- Rename the package scope
|/  
*   4aa95d5 -- Merge branch '17-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d016790 -- Resolve "Migrate from tslint to eslint"
|/  
*   e2ef594 -- Merge branch '445720-master-patch-02194' into 'master'
|\  
| * 5ecd26b -- Update README.md
|/  
* e6f7783 -- Update project package.json version based on GitLab tag. Done by CI
*   6b7669f -- Merge branch '16-rename-package-to-kypo-sandbox-model' into 'master'
|\  
| * cff4c9d -- Rename the package
|/  
* f394fa7 -- Update project package.json version based on GitLab tag. Done by CI
*   27ba748 -- Merge branch '15-add-virtual-images-models' into 'master'
|\  
| * 822b5a3 -- Resolve "Add virtual images models"
|/  
* e3dce87 -- Update project package.json version based on GitLab tag. Done by CI
*   72a21f1 -- Merge branch '14-add-sandbox-resources-models' into 'master'
|\  
| * b133e56 -- Resolve "Add sandbox resources models"
|/  
*   56f4143 -- Merge branch '13-use-cypress-image-in-ci' into 'master'
|\  
| * 7fd6cfe -- Resolve "Use cypress image in CI"
|/  
* 3706d0a -- Update project package.json version based on GitLab tag. Done by CI
*   0818c2e -- Merge branch '12-refactor-to-use-sentinel-common-instead-of-kypo-common' into 'master'
|\  
| * 1516547 -- Remove dependency on kypo-common
|/  
*   7b48a39 -- Merge branch '11-remove-allocation-stage-id-from-openstack-cleanup-stage' into 'master'
|\  
| * 6dc6a30 -- Remove allocation stage id attr
|/  
* 67236b5 -- Update project package.json version based on GitLab tag. Done by CI
*   743f8ea -- Merge branch '10-add-new-stage-types' into 'master'
|\  
| * 72b3b4f -- Resolve "Add new stage types"
|/  
* 69ab271 -- Update project package.json version based on GitLab tag. Done by CI
*   1479e53 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 3fa2375 -- Resolve "Update to Angular 10"
|/  
* fbe7b31 -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
* b8158c7 -- Update .gitlab-ci.yml
### 12.0.2 Update gitlab CI config
* 6c7c99c -- [CI/CD] Update packages.json version based on GitLab tag.
*   44c03e4 -- Merge branch '24-rollback-version' into 'master'
|\  
| * 8fba612 -- Update release message
|/  
*   bc00dbf -- Merge branch '23-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 4cd410b -- Update gitlab CI
|/  
* 4e4a557 -- Update project package.json version based on GitLab tag. Done by CI
*   89cc966 -- Merge branch '22-update-to-angular-12' into 'master'
|\  
| * 5413b5d -- Resolve "Update to Angular 12"
|/  
* 21711e5 -- Update project package.json version based on GitLab tag. Done by CI
*   25a5976 -- Merge branch '21-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 821c4be -- peerDependency update
|/  
* 4b2d2c0 -- Update project package.json version based on GitLab tag. Done by CI
*   278d08a -- Merge branch '20-update-to-angular-11' into 'master'
|\  
| * dcc5fb7 -- Resolve "Update to Angular 11"
|/  
*   628c498 -- Merge branch '19-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * c016f61 -- recreate package-lock
|/  
* f0836c8 -- Update project package.json version based on GitLab tag. Done by CI
*   fa45b85 -- Merge branch '18-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * aa15285 -- Rename the package scope
|/  
*   4aa95d5 -- Merge branch '17-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d016790 -- Resolve "Migrate from tslint to eslint"
|/  
*   e2ef594 -- Merge branch '445720-master-patch-02194' into 'master'
|\  
| * 5ecd26b -- Update README.md
|/  
* e6f7783 -- Update project package.json version based on GitLab tag. Done by CI
*   6b7669f -- Merge branch '16-rename-package-to-kypo-sandbox-model' into 'master'
|\  
| * cff4c9d -- Rename the package
|/  
* f394fa7 -- Update project package.json version based on GitLab tag. Done by CI
*   27ba748 -- Merge branch '15-add-virtual-images-models' into 'master'
|\  
| * 822b5a3 -- Resolve "Add virtual images models"
|/  
* e3dce87 -- Update project package.json version based on GitLab tag. Done by CI
*   72a21f1 -- Merge branch '14-add-sandbox-resources-models' into 'master'
|\  
| * b133e56 -- Resolve "Add sandbox resources models"
|/  
*   56f4143 -- Merge branch '13-use-cypress-image-in-ci' into 'master'
|\  
| * 7fd6cfe -- Resolve "Use cypress image in CI"
|/  
* 3706d0a -- Update project package.json version based on GitLab tag. Done by CI
*   0818c2e -- Merge branch '12-refactor-to-use-sentinel-common-instead-of-kypo-common' into 'master'
|\  
| * 1516547 -- Remove dependency on kypo-common
|/  
*   7b48a39 -- Merge branch '11-remove-allocation-stage-id-from-openstack-cleanup-stage' into 'master'
|\  
| * 6dc6a30 -- Remove allocation stage id attr
|/  
* 67236b5 -- Update project package.json version based on GitLab tag. Done by CI
*   743f8ea -- Merge branch '10-add-new-stage-types' into 'master'
|\  
| * 72b3b4f -- Resolve "Add new stage types"
|/  
* 69ab271 -- Update project package.json version based on GitLab tag. Done by CI
*   1479e53 -- Merge branch '9-update-to-angular-10' into 'master'
|\  
| * 3fa2375 -- Resolve "Update to Angular 10"
|/  
* fbe7b31 -- Merge branch '8-make-the-ci-build-stage-build-with-prod-param' into 'master'
* b8158c7 -- Update .gitlab-ci.yml
